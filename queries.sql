-- Adding a key because there are duplicate player names (the Sebastian Ahos)
alter table nhl_stats  add column player_key varchar(255) default null;
update nhl_stats set player_key = "Player" || "Pos";

-- Adding a key for the same reason as above + traded players appear on each team they played on
alter table nhl_bio  add column player_key varchar(255) default null;
update nhl_bio set player_key = first_name || ' ' || last_name || substring(position_abbreviation, 1,1) ;


-- Select interesting features from nhl_bio
-- Most average player has the most average height, weight (by position) and age
select
*
from 
(
	select 
		player_key,
		first_name || ' ' || last_name as player_name,
		DATE_PART('year', now()) - DATE_PART('year', to_date(birth_date, 'YYYY-MM-DD' ))  as current_age,
		 left(height, 1)::real * 12 + substring(split_part(height, '''', 2),0, length(split_part(height, '''', 2)))::real as height_in,
		 weight as weight_lbs,
		 case
		 	when position_abbreviation in ('C', 'RW', 'LW') then 'F' 
		 	else position_abbreviation
		 	end 
		 as pos
	from nhl_bio
	where position_abbreviation <> 'G'
) as nhl_bio
group by player_key, player_name, current_age, height_in, weight_lbs, pos

;

select * from nhl_stats;
-- Select interesting features from nhl_stats ns 
-- 
with nhl_stats2 as (
	select * 
	from 
	(
		select 
		player_key,
		"Player" as player_name,
		case when "Pos" in ('C', 'L', 'R') then 'F' else "Pos" end as pos,
		"G"::real/"GP" as g_pg,
		"A"::real/"GP" as a_pg,
		"+/-" as plus_minus,
		"PIM"::real/"GP" as pim_pg,
		"EVG"::real/"GP" as evg_pg,
		("EVP"::real - "EVG"::real)/"GP" as eva_pg,
		"PPG"::real/"GP" as ppg_pg,
		("PPP"::real - "PPG"::real)/"GP" as ppa_pg,
		"SHG"::real/"GP" as shg_pg,
		("SHP"::real - "SHG"::real)/"GP" as sha_pg,
		"GWG"::real/"GP" as gwg_pg,
		"S"::real/"GP" as s_pg,
		split_part("TOI/GP", ':', 1)::real * 60 + split_part("TOI/GP", ':', 2)::real as toi_pg
		from nhl_stats 
		where "GP" > 41
	) as nhl_stats
	group by player_key, player_name, pos, g_pg, a_pg, plus_minus, pim_pg, evg_pg, eva_pg, ppg_pg, ppa_pg, shg_pg,sha_pg, gwg_pg, s_pg, toi_pg
),
--
nhl_bio2 as 
(
select
*
from 
(
	select 
		player_key,
		first_name || ' ' || last_name as player_name,
		DATE_PART('year', now()) - DATE_PART('year', to_date(birth_date, 'YYYY-MM-DD' ))  as current_age,
		 left(height, 1)::real * 12 + substring(split_part(height, '''', 2),0, length(split_part(height, '''', 2)))::real as height_in,
		 weight as weight_lbs,
		 case
		 	when position_abbreviation in ('C', 'RW', 'LW') then 'F' 
		 	else position_abbreviation
		 	end 
		 as pos
	from nhl_bio
	where position_abbreviation <> 'G'
) as nhl_bio
group by player_key, player_name, current_age, height_in, weight_lbs, pos
),
--
average as (
	select  
		nhl_stats2.pos,  
		avg(g_pg) as avg_g,
		avg(a_pg) as avg_a,
		avg(plus_minus) as avg_plus_minus,
		avg(pim_pg) as avg_pim,
		avg(evg_pg) as avg_evg,
		avg(eva_pg) as avg_eva,
		avg(ppg_pg) as avg_ppg,
		avg(ppa_pg) as avg_ppa,
		avg(shg_pg) as avg_shg,
		avg(sha_pg) as avg_sha,
		avg(gwg_pg) as avg_gwg,
		avg(s_pg) as avg_s,
		avg(toi_pg) as avg_toi,
		avg(height_in) as avg_height_in,
		avg(weight_lbs) as avg_weight_lbs,
		avg(current_age) as avg_age
	from nhl_stats2
	left join nhl_bio2 on nhl_stats2.player_key = nhl_bio2.player_key
	group by nhl_stats2.pos
)
-- 
select 
nhl_stats2.*,
nhl_bio2.height_in,
nhl_bio2.weight_lbs,
nhl_bio2.current_age
from nhl_stats2
left join nhl_bio2 on nhl_stats2.player_key = nhl_bio2.player_key

;

