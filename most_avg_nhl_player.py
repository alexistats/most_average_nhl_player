import pandas as pd
import pandas.io.sql as sqlio
import logging
import psycopg2
from db_connection import config

logging.basicConfig(level=logging.INFO, filename='logs/most_avg_nhl_player.log')

## First, load the data in a pandas dataframe
params = config()
logging.info("QUERYING DATABASE..")

query = """
select * from nhl_stats;
-- Select interesting features from nhl_stats ns 
-- 
with nhl_stats2 as (
	select * 
	from 
	(
		select 
		player_key,
		"Player" as player_name,
		case when "Pos" in ('C', 'L', 'R') then 'F' else "Pos" end as pos,
		"G"::real/"GP" as g_pg,
		"A"::real/"GP" as a_pg,
		"+/-" as plus_minus,
		"PIM"::real/"GP" as pim_pg,
		"EVG"::real/"GP" as evg_pg,
		("EVP"::real - "EVG"::real)/"GP" as eva_pg,
		"PPG"::real/"GP" as ppg_pg,
		("PPP"::real - "PPG"::real)/"GP" as ppa_pg,
		"SHG"::real/"GP" as shg_pg,
		("SHP"::real - "SHG"::real)/"GP" as sha_pg,
		"GWG"::real/"GP" as gwg_pg,
		"S"::real/"GP" as s_pg,
		split_part("TOI/GP", ':', 1)::real * 60 + split_part("TOI/GP", ':', 2)::real as toi_pg
		from nhl_stats 
		where "GP" > 41
	) as nhl_stats
	group by player_key, player_name, pos, g_pg, a_pg, plus_minus, pim_pg, evg_pg, eva_pg, ppg_pg, ppa_pg, shg_pg,sha_pg, gwg_pg, s_pg, toi_pg
),
--
nhl_bio2 as 
(
select
*
from 
(
	select 
		player_key,
		first_name || ' ' || last_name as player_name,
		DATE_PART('year', now()) - DATE_PART('year', to_date(birth_date, 'YYYY-MM-DD' ))  as current_age,
		 left(height, 1)::real * 12 + substring(split_part(height, '''', 2),0, length(split_part(height, '''', 2)))::real as height_in,
		 weight as weight_lbs,
		 case
		 	when position_abbreviation in ('C', 'RW', 'LW') then 'F' 
		 	else position_abbreviation
		 	end 
		 as pos
	from nhl_bio
	where position_abbreviation <> 'G'
) as nhl_bio
group by player_key, player_name, current_age, height_in, weight_lbs, pos
)
-- 
select 
nhl_stats2.*,
nhl_bio2.height_in,
nhl_bio2.weight_lbs,
nhl_bio2.current_age
from nhl_stats2
left join nhl_bio2 on nhl_stats2.player_key = nhl_bio2.player_key
"""

conn = psycopg2.connect(**params)
cur = conn.cursor()

## first, get both datasets in pandas
data = sqlio.read_sql_query(query, conn)

cur.close()

## Computing the means, standard deviations and how many standard deviation from the mean each player is for each metric
logging.info("CALCULATING THE MEANS AND STANDARD DEVIATION OF EACH METRIC..")

means = data.groupby(by="pos").mean()
medians = data.groupby(by="pos").median()
stds = data.groupby(by="pos").std()

data = data.set_index('player_name')
## Set to dictionaries
players = data.to_dict(orient='index')
means = means.to_dict()
medians = medians.to_dict()
stds = stds.to_dict()

list_of_distances = []
list_of_distances_median = []
## Loop over all players to find how far they are from the mean - output player with minimum distance value
for player_name, attributes in players.items():
    pos = attributes['pos']
    distances = []
    distances_median = []
    for metric in means:
        mean = means[metric][pos]
        std = stds[metric][pos]
        median = medians[metric][pos]
        player_value = attributes[metric]

        ## Calc how far from mean based on standard deviation
        dist_from_mean = abs(player_value - mean)/std
        dist_from_median = abs(player_value - median)/std
        distances.append(dist_from_mean)
        distances_median.append(dist_from_median)

    ## Sum total deviation from means and save in dict
    attributes['distance_from_mean'] = sum(distances)
    attributes['distance_from_median'] = sum(distances_median)

    list_of_distances.append([player_name, pos, attributes['distance_from_mean']])
    list_of_distances_median.append([player_name, pos, attributes['distance_from_median']])


list_of_distances = sorted(list_of_distances, key=lambda x: x[2])
list_of_distances_median = sorted(list_of_distances_median, key=lambda x: x[2])

